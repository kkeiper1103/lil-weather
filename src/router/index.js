import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home.vue'
import SettingsPage from "@/views/SettingsPage";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,

    meta: {
      title: "Lil' Weather Home"
    }
  },

  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),

    meta: {
      title: "Lil' Weather About"
    }
  },

  {
    path: "/settings",
    name: "Settings",
    component: SettingsPage,

    meta: {
      title: "Lil' Weather Settings Page"
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// global "after" hook that sets the page's title
router.afterEach((to) => {
  document.title = to.meta.title || "Lil' Weather";
})

export default router
