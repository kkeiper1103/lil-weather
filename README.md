# Lil' Weather - Home Weather App

## Prereqs
Be sure to register for an API Key from OpenWeatherMap.org. Create an account, then register a free api key with them.

[Register](https://home.openweathermap.org/users/sign_up)

[Create API Key](https://home.openweathermap.org/api_keys)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
